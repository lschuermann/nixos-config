# An extremly fancy wireless-configuration using only wpa_supplicant
{ config, pkgs, ... }:
{
  # Enable wireless support via wpa_supplicant
  networking.wireless = {
    enable = true;

    networks = {
    };
  };
}
