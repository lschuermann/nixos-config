# Edit this configuration file to define what should be installed on
# your system.  Help is available in the configuration.nix(5) man page
# and in the NixOS manual (accessible by running ‘nixos-help’).

{ config, pkgs, ... }:

{
  imports =
    [ # Include the results of the hardware scan.
      ../../hardware-configuration.nix
      ../classes/laptop.nix

      ../services/gui.nix

      # This one imports all of the other, shared wifi networks
      ../services/wireless.nix
    ];

  # Hardware specific options
  hardware.cpu.intel.updateMicrocode = true;

  # Use the systemd-boot EFI boot loader.
  boot.loader.systemd-boot.enable = true;
  boot.loader.efi.canTouchEfiVariables = true;
  
  # Encrypted eMMC
  boot.initrd.luks.devices = [
    {
      name = "cryptmmc";
      device = "/dev/mmcblk0p2";
      preLVM = true;
      allowDiscards = true;
    }
  ];

  networking = {
    hostName = "sulfur";
    
    firewall = {
      enable = true;
      allowedTCPPorts = [ 8080
        445 139 # Samba
      ];
      allowedUDPPorts = [
        137 138 # Samba
      ];
      allowPing = true;
    };
  };

  # Unleash the full power of the NETBOOK!
  nix.buildCores = 2; 

  i18n = {
    consoleFont = "Lat2-Terminus16";
    consoleKeyMap = "us";
    defaultLocale = "en_US.UTF-8";
  };

  # Set your time zone.
  time.timeZone = "Europe/London";

  # Use fish also for root
  # Don't enable this on all systems, could cause trouble on servers
  programs.fish.enable = true;
  users.extraUsers.root.shell = pkgs.fish;

  services.openssh = {
    enable = true;
    openFirewall = true;
    forwardX11 = true;
  };

  services.ntp.enable = true;

  users.extraUsers.leons.openssh.authorizedKeys.keys = [
    "ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAINwX/847rz7k+9cqVSTH0q4yZTUPn/DGzK8cxQVNhJmB lschuermann/hpe/elitebook840"
  ];

  # This value determines the NixOS release with which your system is to be
  # compatible, in order to avoid breaking some software such as database
  # servers. You should change this only after NixOS release notes say you
  # should.
  system.stateVersion = "18.03"; # Did you read the comment?

  #TODO: GENERIC
  boot.kernelModules = [ "exfat" ];
}
