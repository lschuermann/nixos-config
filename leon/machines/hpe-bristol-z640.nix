# Edit this configuration file to define what should be installed on
# your system.  Help is available in the configuration.nix(5) man page
# and in the NixOS manual (accessible by running ‘nixos-help’).

{ config, pkgs, ... }:
let

  decrypt = (import ../../lib/openssl_decrypt.nix) pkgs;

in
{
  imports =
    [ # Include the results of the hardware scan.
      ../../hardware-configuration.nix

      ../classes/desktop.nix
    ];

  # Hardware specific options
  hardware.cpu.intel.updateMicrocode = true;

  # Use the systemd-boot EFI boot loader.
  boot.loader.systemd-boot.enable = true;
  boot.loader.efi.canTouchEfiVariables = true;

  networking = {
    firewall = {
      enable = true;
      allowedTCPPorts = [
        8080
        445 139 # Samba
      ];
      allowedUDPPorts = [
        137 138 # Samba
      ];
      allowPing = true;
    };

    hostName = "schurman-workstation0.uk.rdlabs.hpecorp.net";
    defaultGateway = "10.37.172.1";
    nameservers = [ "16.110.135.52" "16.110.135.51" ];
    interfaces.eno1 = {
      ipv4.addresses = [ { address = "10.37.172.64"; prefixLength = 22; } ];
    };
  };

  # This is a workstation with 16 (hyperthreaded) cores
  nix.buildCores = 16; 

  i18n = {
    consoleFont = "Lat2-Terminus16";
    consoleKeyMap = "us";
    defaultLocale = "en_US.UTF-8";
  };

  # Set your time zone.
  time.timeZone = "Europe/London";

  systemd.services.reverse-ssh = {
    description = "Reverse SSH Service - Backup tunnel";
    serviceConfig = {
      User = "root";
      Group = "root";
      Type = "simple";
      ExecStart = "${pkgs.openssh}/bin/ssh -NTC -o ServerAliveInterval=60 -o ExitOnForwardFailure=yes -o StrictHostKeyChecking=no -o UserKnownHostsFile=no -i /var/reverse_ssh_key -R 49002:localhost:22 rev-ssh@xenon.rev-ssh.currently.online";
      RestartSec = "5";
      Restart = "always";
    };
    wantedBy = [ "multi-user.target" ];
  };

  programs.fish.enable = true;
  users.extraUsers.root = {
    hashedPassword = "$6$HdVnd2p/CHMvBr$6lAYaN9GPRgdJOPdutZaQnoX4dhXwy9YDvRXwSMd8y0M0ifJGb4MpKrflfj6ZiruPNvRgmjAuB57.cX.WHeOg1";
    shell = pkgs.fish;
  };

  services.openssh = {
    enable = true;
    openFirewall = true;
    forwardX11 = true;
  };

  programs.ssh = {
    startAgent = true;
    agentTimeout = null;
    package = pkgs.openssh;
  };

  users.mutableUsers = false;
  users.extraUsers.leons = {
    hashedPassword = "$6$HdVnd2p/CHMvBr$6lAYaN9GPRgdJOPdutZaQnoX4dhXwy9YDvRXwSMd8y0M0ifJGb4MpKrflfj6ZiruPNvRgmjAuB57.cX.WHeOg1";
    openssh.authorizedKeys.keys = [
      # TODO: Add some here
      "ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAINwX/847rz7k+9cqVSTH0q4yZTUPn/DGzK8cxQVNhJmB lschuermann/hpe/elitebook840"
    ];
  };

  networking.proxy.default =
    # Let's abuse the wireless secret for now
    decrypt
      ../../secrets/wireless_secret
      "corporate_proxy"
      "U2FsdGVkX19+XxL74BKAGWL4+Qm/9W09S3aXc42IZLpKOwgH5t2F/hpHGMEEAkU1EPWcCq1Vm+sEYKs45vP0Nw==";

  boot.kernelModules = [ "kvm-intel" ];
  virtualisation = {
    libvirtd = {
      enable = true;
      qemuOvmf = true;
    };
  };


  # This value determines the NixOS release with which your system is to be
  # compatible, in order to avoid breaking some software such as database
  # servers. You should change this only after NixOS release notes say you
  # should.
  system.stateVersion = "18.03"; # Did you read the comment?

}
