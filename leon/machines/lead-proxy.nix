# Edit this configuration file to define what should be installed on
# your system.  Help is available in the configuration.nix(5) man page
# and in the NixOS manual (accessible by running ‘nixos-help’).

{ config, pkgs, ... }:

{
  imports =
    [ # Include the results of the hardware scan.
      ../../hardware-configuration.nix
      ../classes/lead-vm.nix
    ];


  # Use the systemd-boot EFI boot loader.
  boot.loader.systemd-boot.enable = true;
  boot.loader.efi.canTouchEfiVariables = true;
  
  networking = {
    hostName = "proxy.lead.is.currently.online";

    interfaces.ens18 = {
      ipv6.addresses = [
        { address = "2a01:4f9:2a:135b:abc:0::2"; prefixLength = 96; }
      ];
      ipv4.addresses = [
        { address = "10.182.0.2"; prefixLength = 30; }
      ];
    };

    defaultGateway6 = "2a01:4f9:2a:135b:abc:0::1";

    firewall.allowedTCPPorts = [ 80 443 ];
  };

  services.nginx = {
    enable = true;
    
    virtualHosts = {
      "cloud.currently.online" = {
        enableACME = true;
        forceSSL = true;

        locations."/" = {
          proxyPass = "http://nextcloud.is.currently.online:80/";
        };
      };
    };
  };

  # This value determines the NixOS release with which your system is to be
  # compatible, in order to avoid breaking some software such as database
  # servers. You should change this only after NixOS release notes say you
  # should.
  system.stateVersion = "18.03"; # Did you read the comment?
}
