# Edit this configuration file to define what should be installed on
# your system.  Help is available in the configuration.nix(5) man page
# and in the NixOS manual (accessible by running ‘nixos-help’).

{ config, pkgs, ... }:

{
  imports =
    [ # Include the results of the hardware scan.
      ../../hardware-configuration.nix
      ../classes/server.nix
    ];

  # Hardware specific options
  hardware.cpu.intel.updateMicrocode = true;

  # Use the systemd-boot EFI boot loader.
  boot.loader.systemd-boot.enable = true;
  boot.loader.efi.canTouchEfiVariables = true;
  
  networking = {
    hostName = "platinum.is.currently.online";
   
    # Let's use DHCP for now
 
    firewall = {
      enable = true;
      allowPing = true;
    };
  };

  systemd.services.reverse-ssh = {
    description = "Reverse SSH Service - Backup tunnel";
    serviceConfig = {
      User = "root";
      Group = "root";
      Type = "simple";
      ExecStart = "${pkgs.openssh}/bin/ssh -NTC -o ServerAliveInterval=60 -o ExitOnForwardFailure=yes -o StrictHostKeyChecking=no -o UserKnownHostsFile=no -i /rev_ssh_key -R 49003:localhost:22 rev-ssh@xenon.rev-ssh.currently.online";
      RestartSec = "5";
      Restart = "always";
    };
    wantedBy = [ "multi-user.target" ];
  };

  # Intel(R) Celeron(R) CPU  J3160  @ 1.60GHz
  nix.buildCores = 4; 

  i18n = {
    consoleFont = "Lat2-Terminus16";
    consoleKeyMap = "us";
    defaultLocale = "en_US.UTF-8";
  };

  # Set your time zone.
  time.timeZone = "Europe/Berlin";

  services.ntp.enable = true;

  # This value determines the NixOS release with which your system is to be
  # compatible, in order to avoid breaking some software such as database
  # servers. You should change this only after NixOS release notes say you
  # should.
  system.stateVersion = "18.03"; # Did you read the comment?
}
