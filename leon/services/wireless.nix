{ config, pkgs, lib, ... }:
let

  decrypt = ((import ../../lib/openssl_decrypt.nix) pkgs) ../../secrets/wireless_secret;

  toWifiAttrSet = wifiList:
    builtins.listToAttrs (map (w:
      {
        name = w.ssid;
        value =
          (lib.filterAttrs (n: _: n != "ssid" && n != "priority") w) //
          {
            priority = if (w ? priority) && w.priority != null then w.priority else 50;
          };
      }
    ) wifiList);

  encWifi = comment: priority: ssid_cyphertext: psk_cyphertext: {
    ssid = decrypt "${comment}_ssid" ssid_cyphertext;
    psk = decrypt "${comment}_psk" psk_cyphertext;
    priority = priority;
  };

in
{
  # Import shared wifi network config
  imports = [ ../../services/wireless.nix ];

  networking.wireless = {
    enable = true;
    networks = toWifiAttrSet [

      # ----- PUBLIC -----
      { ssid = "PretCustomer"; }


      # ----- TEMPORARY -----
      (encWifi
        "airbnb_bristol" 80
        "U2FsdGVkX1+QvIPpbbSn7swwFJsKJ5YLCAhnmlzrJjE="
        "U2FsdGVkX18SWDZss/9FLtCPItN3tBOxVBj1OAyiSLU=")

      # ----- PRIMARY -----
      (encWifi
        "home_siegen" 80
        "U2FsdGVkX19RC98Nl8FkhiLSs9BAkYg0bicZFNIVFcgXmXY3rHfsDVu1fv24OUIJ"
        "U2FsdGVkX1+ngWNz3tPAgGmz5N84lv62EgBtAYAqHOiRkDmjDiDNbLyBDv0kkBbp")
      (encWifi
        "home_stuttgart" 80
        "U2FsdGVkX19RC98Nl8FkhiLSs9BAkYg0bicZFNIVFcgXmXY3rHfsDVu1fv24OUIJ"
        "U2FsdGVkX1+ngWNz3tPAgGmz5N84lv62EgBtAYAqHOiRkDmjDiDNbLyBDv0kkBbp")

      # ----- OTHER PLACES -----
      (encWifi
        "hackerspace_siegen" 70
        "U2FsdGVkX19GuOIJHbAWyXsbXFROEYoUCimniTwzWX7VLFXjr9Pqcr25V+Ykmbu1"
        "U2FsdGVkX18mNUl3+mvb1zrGDgL9gTkjP3K9qje0OwA=")

      # University Wifi
      { ssid = "eduroam";
        priority = 70;
        auth = ''
          key_mgmt=WPA-EAP
          identity="${decrypt "eduroam_username"
            "U2FsdGVkX18O8C3UKb3tLcRxTkYO7aMab02ocRdAkXG5xKHfm7ZWN6XJR7WeseEvizPAkbXicCyQiLTuvfYPtg=="}"
          password="${decrypt "eduroam_passphrase" "U2FsdGVkX1/9Sh9VGu2IMRnSpLl3BnIXeVvYoMQeFqs="}"
          eap=PEAP
          phase1="peaplabel=0"
          phase2="auth=MSCHAPV2"
        '';
      }

      # Company employee network
      { ssid = decrypt "company_employee_ssid" "U2FsdGVkX1+YoJFbtL7k9zxGFXAD/t+LdmLCfasQaXY=";
        priority = 60;
        auth = ''
          key_mgmt=WPA-EAP
          identity="${decrypt "company_employee_username"
            "U2FsdGVkX1+KPXEz+0dqA+3kZ224CHGpldTKxRryQF2yl1XFFuCjuKGrWq8IOket"}"
          password="${decrypt "company_employee_passphrase" "U2FsdGVkX1/amufb126+TIOYBSCdOAz+HoaPedbqvHY="}"
          eap=PEAP
          phase1="peaplabel=0"
          phase2="auth=MSCHAPV2"
        '';
      }

    ];
  };
}
