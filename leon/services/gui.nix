# Edit this configuration file to define what should be installed on
# your system.  Help is available in the configuration.nix(5) man page
# and in the NixOS manual (accessible by running ‘nixos-help’).

{ config, pkgs, ... }:

{
  imports =
    [ ../programs/i3.nix
    ];

  # List packages installed in system profile. To search by name, run:
  # $ nix-env -qaP | grep wget
  environment.systemPackages = with pkgs; [
    chromium firefox xfce.terminal gnome3.networkmanagerapplet arandr keepassxc i3lock-fancy scrot maim xorg.xbacklight xorg.xev pavucontrol alsaTools
  ];
  sound.enable = true;

  fonts.fonts = with pkgs; [ font-awesome-ttf fantasque-sans-mono ];

  # Allow unfree packages (required for spotify)
  nixpkgs.config.allowUnfree = true;


  services.logind = {
    lidSwitch = "suspend";

    # This option is required on argon; probably hw bug
    lidSwitchDocked = "suspend";

    extraConfig = ''
      HandlePowerKey=ignore
    '';
  };

  #systemd.services.lock-mute = {
  #  description = "Mute the audio before suspend";
  #  serviceConfig = {
  #    ExecStart = ''${pkgs.busybox}/bin/sh -c "${config.hardware.pulseaudio.package.out}/bin/pactl set-sink-mute @DEFAULT_SINK@ 1 || true"'';
  #    Type = "oneshot";
  #    User = "leons";
  #  };
  #  wantedBy = [ "sleep.target" "suspend.target" ];
  #  before = [ "lock-suspend.service" "sleep.target" "suspend.target" ];
  #  enable = true;
  #};

  #systemd.services.lock-suspend = {
  #  description = "Lock the screen using i3lock-fancy on suspend";
  #  serviceConfig = {
  #    ExecStart = ''${pkgs.i3lock-fancy}/bin/i3lock-fancy -p'';
  #    ExecStartPost = ''${pkgs.busybox}/bin/sh -c "sleep 2"'';
  #    Environment = "DISPLAY=:0";
  #    User = "leons";
  #  };
  #  wantedBy = [ "sleep.target" "suspend.target" ];
  #  before = [ "sleep.target" "suspend.target" ];
  #  enable = true;
  #};

  # Enable CUPS to print documents.
  services.printing.enable = true;

  hardware.pulseaudio.enable = true;

  services.xserver = {
    enable = true;

    layout = "us";
    xkbVariant = "mac";
    xkbOptions = "caps:escape";

    libinput.enable = true;
    
    displayManager.lightdm = {
      enable = true;
    };

    windowManager.i3 = {
      enable = true;
      extraPackages = with pkgs; [ dmenu i3status-rust ];
    };
  };

  # Define a user account. Don't forget to set a password with ‘passwd’.
  users.extraUsers.leons = {
    isNormalUser = true;
    uid = 1000;
    extraGroups = [ "sudo" "dialout" ];
  };
}
