{ pkgs, ... }:

{
  imports = [ ./server.nix ];

  users.extraUsers.root = {
    openssh.authorizedKeys.keys = [
      # Important, should be installed on every vm
      "ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIDUho/ppgkXeIu4SARVwcmvMcR9RkAiRiiPEHl3am8MR lead/shared_vms_key"
    ];
  };

  # Use cloudflare's public DNS for now, could change this for a selfhosted one
  networking.nameservers = [ "2606:4700:4700::1111" "2606:4700:4700::1001" ];
  
  time.timeZone = "UTC";

  boot.loader.grub = {
    enable = true;
    version = 2;
    device = "/dev/vda";
  };
}
