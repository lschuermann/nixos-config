{ config, pkgs, ... }:

{
  imports = [
    ../programs/tmux.nix
    ../programs/fish.nix
  ];
 
  environment.systemPackages = with pkgs; [
    # Packages I can't live without
    wget vim htop git socat gnupg gptfdisk woof

    # We need this on most systems
    direnv mosh
  ];

  # Sane sudo config
  # We want to give users in group 'wheel' passwordless sudo
  # and those in group 'sudo' password-enabled sudo
  users.extraGroups = { sudo = {}; };
  security.sudo = {
    enable = true;

    wheelNeedsPassword = false;
    extraRules = [
      { groups = [ "sudo" ]; commands = [ "ALL" ]; }
    ];
  };

  # Enable manpage support
  programs.man.enable = true;

  # Show the nixos manual on tty 8
  services.nixosManual = {
    enable = true;
    ttyNumber = 8;
  };
}
