{ config, pkgs, ... }:

{
  imports = [
    ./base.nix
    ../services/gui.nix
  ];
}
