{ server, pkgs, ... }:

{
  imports = [ ./base.nix ];

  networking = {
    # SANE DEFAULTS!!!
    firewall.enable = true;

    dhcpcd.enable = false;
  };

  # Let's just run an openssh server on the default port
  services.openssh = {
    enable = true;

    # We don't want to lock us out accidentally
    openFirewall = true;

    permitRootLogin = "prohibit-password";
  };

  users.extraUsers.root = {
    openssh.authorizedKeys.keys = [
      # Important, should be installed on every system
      "ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIKMpinImAJaJzmNEDF1sjUm5zrloD2oIA6RsdiOOJoT6 lschuermann/ssh-backup-key"
    ];
  };
}
