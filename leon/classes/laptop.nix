{ config, pkgs, ... }:

{
  imports = [
    ./base.nix
    ../services/gui.nix
  ];

  services.xserver.libinput = {
    enable = true;
    tapping = false;
  };
}
