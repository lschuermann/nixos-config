{ config, pkgs, ... }:
{
  imports =
    [ # Include the results of the hardware scan.
      ../../modules/files.nix
    ];

  environment.systemPackages = [ pkgs.tmux ];

  users.extraUsers.leons.files = [ {
    dest = ".tmux.conf";

    # Could also use a path in the nix store here
    # src = "${pkgs.openssh}/bin/ssh";
    src = ./tmux.conf;

    # Using content works too, bot not together with src
    # content = "...";
  } ];
}
