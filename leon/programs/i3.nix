{ config, pkgs, ... }:
{
  imports = [ ../../modules/files.nix ];

  environment.systemPackages = with pkgs; [ i3 i3status-rust ];

  users.extraUsers.leons = {
    files = [ {
      dest = ".config/i3/config";
      src = ./i3_config;
    } {
      dest = ".config/i3/i3status-rs.toml";
      src = ./i3status-rs.toml;
    } {
      dest = ".config/i3/increase_volume";
      src = ./i3_increase_volume;
    } {
      dest = ".config/i3/toggle_touchpad";
      src = ./i3_toggle_touchpad;
    } ];
  };
}
