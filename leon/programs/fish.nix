{ config, pkgs, ... }:
{
  imports = [ ../../modules/files.nix ];

  environment.systemPackages = [ pkgs.fish ];

  programs.fish = {
    enable = true;
  };

  users.extraUsers.leons = {
    shell = pkgs.fish;

    files = [ {
      dest = ".config/fish/config.fish";
      src = ./fish_config.fish;
    } ];
  };
}
